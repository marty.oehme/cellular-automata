import 'package:rules_of_living/src/rules/CellPattern.dart';

abstract class RuleSet {
  int range;
  List<CellPattern> patterns;

  bool checkSurvival(int neighbors);
  bool checkBirth(int neighbors);
}
