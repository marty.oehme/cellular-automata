import 'package:test/test.dart';
import 'dart:html' as html;

void main() {
  test("dart_test works", () {
    expect(4+4, equals(8));
  });

  test("dart_test works with the browser", () {
    expect(html.Document, equals(isNotNull));
  });
}