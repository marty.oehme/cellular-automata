import 'dart:html' as html;
import 'dart:math';

import 'package:rules_of_living/service/engine_service.dart';
import 'package:rules_of_living/src/Simulation.dart';

class SimulationService {
  // DEFAULT VALUES
  static final int DEFAULT_GRID_SIZE = 50;

  final EngineService _engine;
  final Simulation _sim;

  SimulationService(this._engine, [Simulation sim])
      : this._sim = sim ?? Simulation(DEFAULT_GRID_SIZE, DEFAULT_GRID_SIZE) {
    _engine.simulation = _sim;
    _sim.addRandomPattern(amount: 15, dispersal: 5);
  }

  void reset() {
    _sim.reset();
  }

  void addRandomPattern() {
    _sim.addRandomPattern();
  }

  Point<int> get gridSize => _sim.gridSize;
  void set gridSize(Point<int> size) {
    _sim.gridSize = size;
  }

  //TODO split into RenderService when rendering is decoupled from engine.
  html.CanvasElement get canvas => _engine.engine.canvas;
  void set canvas(html.CanvasElement canvas) => _engine.engine.canvas = canvas;

  void toggleGrid() {
    _sim.renderEdges = !_sim.renderEdges;
  }

  void save() => _sim.saveSnapshot();
  void load() => _sim.loadSnapshot();
}
