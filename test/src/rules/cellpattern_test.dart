import 'dart:math';

import 'package:rules_of_living/src/rules/CellPattern.dart';
import 'package:test/test.dart';

void main() {
  CellPattern sut;
  setUp(() {
    sut = CellPattern("testPattern", [Point(1, 1), Point(0, 0), Point(-1, -1)]);
  });
  group("Naming", () {
    test("contains the name passed in for name variable",
        () => expect(sut.name, "testPattern"));
    test(
        "Contains the name passed in on being formatted as String",
        () => expect(sut.toString(),
            "testPattern: [Point(1, 1), Point(0, 0), Point(-1, -1)]"));
  });
}
