import 'dart:math';

import 'package:collection/collection.dart';

class CellPattern extends DelegatingList<Point> {
  final String _name;
  CellPattern(String name, List<Point> base)
      : _name = name,
        super(base);

  String get name => _name;

  @override
  String toString() {
    return "$name: ${super.toString()}";
  }
}
