import 'package:rules_of_living/src/Engine.dart';
import 'package:rules_of_living/src/Simulation.dart';

class EngineService {
  Engine _uncachedEngineAccess;

  EngineService() {
    simSpeed = 5;
  }

  Engine get engine => _uncachedEngineAccess ?? _setCachedAndReturn(Engine());
  void set engine(Engine newEngine) {
    _uncachedEngineAccess = newEngine;
  }

  Engine _setCachedAndReturn(Engine newEngine) {
    engine = newEngine;
    return newEngine;
  }

  void run() {
    engine.running = true;
  }

  void stop() {
    engine.running = false;
  }

  void toggleRunning() {
    engine.running = !engine.running;
  }

  void step() {
    engine.step();
  }

  /// Simulation Speed
  ///
  /// Sets the number of updates the simulation takes per second. Can range from
  /// 1 to arbitrarily high numbers (though setting it too high can potentially
  /// make the app brittle).
  int get simSpeed => engine.stepsPerSecond;
  void set simSpeed(int val) => engine.stepsPerSecond = val;

  bool get isRunning => engine.running;

  void set simulation(Simulation value) => engine.simulation = value;
}
