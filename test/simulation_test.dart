import 'dart:math';
import 'package:mockito/mockito.dart';
import 'package:rules_of_living/src/Grid.dart';
import 'package:test/test.dart';

import 'package:rules_of_living/src/Simulation.dart';

class MockGrid extends Mock implements Grid<bool> {}

void main() {
  Simulation sut;
  setUp(() {
    sut = Simulation(10, 10);
  });
  group("gridSize", () {
    test(
        "returns the width and height of the underlying grid",
        () => expect(
            sut.gridSize, equals(Point<int>(sut.map.width, sut.map.height))));
    test("sets the underlying grid width and height", () {
      sut.gridSize = Point(2, 3);
      expect(sut.gridSize, equals(Point(2, 3)));
    });
    test("creates a new underlying grid on resizing", () {
      var oldMap = sut.map;
      sut.gridSize = Point(10, 10);
      expect(sut.map, isNot(oldMap));
    });
  });
  group("reset", () {
    test("returns a map filled with 'false' ", () {
      expect(sut.reset(), allOf(TypeMatcher<Grid>(), isNot(contains(true))));
    });
    test("sets the simulation to need re-rendering", () {
      sut.reset();
      expect(sut.dirty, true);
    }, skip: "can not find a way to set dirty to true first yet");
  });
  group("save&load", () {
    test(
        "saves a copy of the map which does not change when the actual map changes",
        () {
      sut.saveSnapshot();
      sut.mergeStateChanges({1: true, 2: true});
      var snapshot = Grid.from(sut.map);

      expect(sut.loadSnapshot(), isNot(equals(snapshot)));
    });
  });
}
