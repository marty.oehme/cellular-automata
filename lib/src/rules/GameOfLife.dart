import 'dart:math';

import 'package:rules_of_living/src/rules/RuleSet.dart';
import 'package:rules_of_living/src/rules/CellPattern.dart';

class GameOfLife implements RuleSet {
  int range = 1;
  List<CellPattern> patterns = <CellPattern>[
    // Two blocks, offset
    // ##
    //   ##
    CellPattern("Blinker", [
      Point(0, 0),
      Point(1, 0),
      Point(0, 1),
      Point(1, 1),
      Point(2, 2),
      Point(3, 2),
      Point(2, 3),
      Point(3, 3)
    ]),
    // A 'gliding' Spaceship
    //   #
    //    #
    //  ###
    CellPattern("SpaceShip", [
      Point(1, 0),
      Point(2, 1),
      Point(2, 2),
      Point(1, 2),
      Point(0, 2),
    ])
  ];

  bool checkSurvival(int neighbors) =>
      neighbors == 2 || neighbors == 3 ? true : false;
  bool checkBirth(int neighbors) => neighbors == 3 ? true : false;
}
