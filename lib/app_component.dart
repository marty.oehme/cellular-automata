import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:rules_of_living/components/configuration_component.dart';
import 'package:rules_of_living/components/controls_component.dart';
import 'package:rules_of_living/components/header_component.dart';
import 'package:rules_of_living/components/simulation_component.dart';
import 'package:rules_of_living/service/engine_service.dart';
import 'package:rules_of_living/service/simulation_service.dart';

@Component(
  selector: 'my-app',
  templateUrl: "app_component.html",
  directives: [
    coreDirectives,
    MaterialButtonComponent,
    MaterialIconComponent,
    MaterialSliderComponent,
    HeaderComponent,
    SimulationComponent,
    ControlsComponent,
    ConfigurationComponent
  ],
  providers: [
    materialProviders,
    ClassProvider(EngineService),
    ClassProvider(SimulationService)
  ],
  styleUrls: const [
    'package:angular_components/app_layout/layout.scss.css',
    'app_component.css'
  ],
)
class AppComponent {
  var name = "World";
}
