import 'dart:math';

import 'package:angular/angular.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_components/material_input/material_input.dart';
import 'package:angular_components/material_input/material_number_accessor.dart';
import 'package:angular_components/material_slider/material_slider.dart';
import 'package:angular_components/material_tooltip/material_tooltip.dart';
import 'package:rules_of_living/service/engine_service.dart';
import 'package:rules_of_living/service/simulation_service.dart';

@Component(
    selector: "configuration",
    templateUrl: "configuration_component.html",
    styleUrls: [
      "configuration_component.css"
    ],
    directives: [
      MaterialButtonComponent,
      MaterialIconComponent,
      MaterialSliderComponent,
      MaterialTooltipDirective,
      materialInputDirectives,
      materialNumberInputDirectives,
      NgModel
    ])
class ConfigurationComponent {
  final EngineService engine;
  final SimulationService sim;

  int get width => sim.gridSize.x;
  void set width(num value) {
    if (value == null || value <= 0) return;
    sim.gridSize = Point(value, sim.gridSize.y);
  }

  int get height => sim.gridSize.y;
  void set height(num value) {
    if (value == null || value <= 0) return;
    sim.gridSize = Point(sim.gridSize.x, value);
  }

  int get simSpeed => engine.simSpeed;
  void set simSpeed(int value) => engine.simSpeed = value;

  String get speedSliderTooltip => "Simulation Speed: $simSpeed";

  ConfigurationComponent(this.engine, this.sim);

  void onEdgesClicked() {
    sim.toggleGrid();
  }
}
